## EVENT PROCESSOR

run with:   

` gradle runEventHandler -PeventsFile=<path/to/events/file`

example: 
To use the sample already included: 

`gradle runEventHandler -PeventsFile=records.log`


Future improvements: 

I have coded this to be simple and fast, it is using a HashMap to store the events until both log lines are read from file and we can calculate the duration. The idea behind this would be to minimize the number of DB operations and support larger files

- because the `sourceCompatibility` is set to `1.7` in the `build.gradle` example I have used a `BufferedReader` instead of Java8 Streams which would make the code simpler and nicer
- dependency injection for HSQL Client rather than passing it to the EventProcessor
- tests