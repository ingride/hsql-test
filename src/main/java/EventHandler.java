import client.HSQLClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processor.EventProcessor;

import java.sql.SQLException;


public class EventHandler {

    public static void main(String[] args) {
        Logger LOGGER = LoggerFactory.getLogger(EventHandler.class);

        if(args.length!=0){
            String logFile = args[0];
            LOGGER.info("Processing events file: ... {}", logFile);
            HSQLClient hsqlClient =  new HSQLClient();
            try {
                hsqlClient.setUp();
                EventProcessor.process(logFile, hsqlClient);
                hsqlClient.closeConnection();
                LOGGER.info("Finished processing {}", logFile);
            } catch (SQLException e) {
                LOGGER.error("Failed to create connection and setup {} " , e);
            }
        }
    }
}
