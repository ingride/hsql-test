package client;

import common.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processor.EventProcessor;

import java.sql.*;

public class HSQLClient {

    private Connection connection;

    private static final String EVENTS_TABLE = "events";
    private static final String CONNECTION_STRING = "jdbc:hsqldb:file:"+ System.getProperty("user.dir") +"/events";
    private static final String USER = "SA";

    static final Logger LOGGER = LoggerFactory.getLogger(HSQLClient.class);

    public HSQLClient() {
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            connection = DriverManager.getConnection(CONNECTION_STRING, USER, "");
        } catch (Exception e) {
            e.printStackTrace(System.out);

        }
    }

    public void setUp() throws SQLException {
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("CREATE TABLE " + EVENTS_TABLE + "( " +
                    "id VARCHAR(20) NOT NULL, "+
                    "alert BOOLEAN DEFAULT FALSE NOT NULL, " +
                    "duration BIGINT NOT NULL, " +
                    "host VARCHAR(20) NULL, " +
                    "type VARCHAR(20) NULL)");
        connection.commit();

    }

    public void insertEvent(Event event, long duration, boolean alert) {
        try {
            Statement stmt = this.connection.createStatement();
            String sql = "INSERT INTO " + EVENTS_TABLE + " VALUES(?, ?, ?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, event.getId());
            ps.setBoolean(2, alert);
            ps.setLong(3, duration);
            ps.setString(4, event.getHost());
            ps.setString(5, event.getType());
            LOGGER.debug("Inserting into {}: {}", EVENTS_TABLE, ps);
            ps.executeUpdate();
            this.connection.commit();
        } catch (SQLException e) {
            LOGGER.error("Failed to insert into {} table {} " , EVENTS_TABLE, e);
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("Failed to close connection {} " , e);
        }
    }
}
