package common;

public class Event {

    private String id;
    private String state;
    private String type = null;
    private String host = null;
    private long timestamp;

    public String getId() {
        return id;
    }

    public void seId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHost() { return host; }

    public void setHost(String host) { this.host = host; }

    public long getTimestamp() {return timestamp; }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isFinished() {
        return this.state == "FINISHED";
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", state=" + state +
                ", type=" + type +
                ", timestamp=" + timestamp +
                ", host='" + host + '\'' +
                '}';
    }
}
