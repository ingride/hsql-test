package processor;

import client.HSQLClient;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import common.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EventProcessor {

    static final Logger LOGGER = LoggerFactory.getLogger(EventProcessor.class);

    private static final int DURATION_THRESHOLD = 3;

    public static void process(String filePath, HSQLClient hsqlClient){
        Map<String, Event> events = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        Event event;

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            for (String line; (line = reader.readLine()) != null;) {
                try {
                    event = mapper.readValue(line, Event.class);
                    if (events.containsKey(event.getId())) {
                        long duration = Math.abs(events.get(event.getId()).getTimestamp() - event.getTimestamp());
                        LOGGER.debug("processing {} event with id {} ", event.getState(), event.getId());
                        hsqlClient.insertEvent(event, duration, isAlert(duration));
                        events.remove(event.getId());
                    } else {
                        events.put(event.getId(), event);
                    }
                }
                catch (JsonGenerationException e) {
                    LOGGER.warn("Invalid line {} . Skipping", line);
                } catch (IOException e) {
                    LOGGER.warn("Invalid line {} . Skipping", line);
                }
            }

        } catch (IOException e) {
            LOGGER.error("Failed to read the file {}", filePath, e);
        }
    }

    private static boolean isAlert( long duration ) {
        return duration >= 4;
    }
}
